import React from "react";
import { Link, withRouter } from "react-router-dom";

const Header = props => {
  const ideasClassName = props.location.pathname.startsWith("/ideas")
    ? "col mr-nav-item -active"
    : "col mr-nav-item";

  const newIdeaClassName =
    props.location.pathname === "/new"
      ? "col mr-nav-item -active"
      : "col mr-nav-item";

  return (
    <div className="mr-header">
      <div className="mr-header-inner row">
        <div className="mr-header-logo col-xs-12 col-sm-6 col-md-4 col-lg-3">
          <Link to="/" className="mr-logo">
            Uzlabo
          </Link>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-8 col-lg-9">
          <nav className="mr-nav">
            <ul className="mr-nav-list row start-xs end-sm end-md end-lg">
              <li className={newIdeaClassName}>
                <Link to="/new" className="mr-btn -green-outline -sm mr-nav-btn">
                  Iesniegt ideju
                </Link>
              </li>
              <li className={ideasClassName}>
                <Link to="/ideas" className="mr-nav-link mr-nav-text">
                  Visas idejas
                </Link>
              </li>
              <li className={ideasClassName}>
                <Link to="#" className="mr-nav-link mr-nav-text">
                  About
                </Link>
              </li>
              <li className={ideasClassName}>
                <button className="mr-nav-link mr-nav-text">
                  Sign in
                </button>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Header);
