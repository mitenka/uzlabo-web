import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/lv";
import {
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
  TelegramShareButton,
  TelegramIcon
} from "react-share";

import { fetchIdea } from "../actions";
import Voting from "./Voting";

moment.locale("lv");

class IdeaDetail extends React.Component {
  componentDidMount() {
    // https://stackoverflow.com/a/39345165/2111934
    window.scrollTo(0, 0);
    this.props.fetchIdea(this.props.match.params.id);
  }

  renderImage(idea) {
    if (idea.images.length === 0) {
      return null;
    }
    return (
      <div className="row mr-card-details">
        <div className="col-xs-12 col-sm-6 col-md-4">
          <div className="mr-card-image">
            <div className="mr-card-image-inner">
              <a
                href={idea.images[0].file}
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={idea.images[0].thumbnail} alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { idea } = this.props;

    if (!idea) {
      return null;
    }

    return (
      <div className="mr-page-inner">
        <header className="row mr-page-header between-xs">
          <h1 className="col mr-page-title">{idea.title}</h1>
          <div className="col mr-page-header-votes">
            <Voting idea={idea} />
          </div>
        </header>

        <div className="row">
          <div className="mr-card col-xs-12 mr-card-content">
            <div className="row mr-card-address">
              <div className="col">
                <span className="mr-icon icon-Map color-greyish" />
                {idea.address}
              </div>
            </div>

            <div className="row">
              <div className="col">
                <span className="mr-card-category -category">
                  {idea.category.name}
                </span>
                <span className="mr-card-category -subcategory">
                  {idea.subcategory.name}
                </span>
              </div>
            </div>

            <div className="row mr-card-description">
              <div className="col">{idea.description}</div>
            </div>

            <div className="row mr-card-info color-greyish">
              <div className="col">
                Pievienots: {moment(idea.created_at).calendar()}
              </div>
              <div className="col">Autors: {idea.author}</div>
            </div>

            {this.renderImage(idea)}

            {/* https://www.embedgooglemap.net/ */}
            <div className="row">
              <div className="col mr-card-map">
                <div className="mr-card-map-inner">
                  <iframe
                    title="idea-detail-map"
                    width="100%"
                    height="100%"
                    frameBorder="0"
                    style={{ border: 0 }}
                    src={`https://maps.google.com/maps?q=${idea.address}&t=&z=16&ie=UTF8&iwloc=&output=embed`}
                    allowFullScreen
                  />
                </div>
              </div>
            </div>

            <div className="row end-xs">
              <div className="col mr-social">
                <div className="mr-social-ico">
                  <TwitterShareButton
                    url={window.location.href}
                    title="Ko Tu domā par manu ieteikumu pilsētas uzlabošanai?"
                    hashtags={["uzlabo"]}
                  >
                    <TwitterIcon size={32} />
                  </TwitterShareButton>
                </div>
                <div className="mr-social-ico">
                  <WhatsappShareButton
                    url={window.location.href}
                    title="Ko Tu domā par manu ieteikumu pilsētas uzlabošanai?"
                  >
                    <WhatsappIcon size={32} />
                  </WhatsappShareButton>
                </div>
                <div className="mr-social-ico">
                  <TelegramShareButton
                    url={window.location.href}
                    title="Ko Tu domā par manu ieteikumu pilsētas uzlabošanai?"
                  >
                    <TelegramIcon size={32} />
                  </TelegramShareButton>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    idea: state.ideas[ownProps.match.params.id]
  };
};

export default connect(mapStateToProps, { fetchIdea: fetchIdea })(IdeaDetail);
