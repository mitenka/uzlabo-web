import React from "react";
import { connect } from "react-redux";
import Address from "./Address";
import {
  fetchCategories,
  setCategoryId,
  setSubcategoryId,
  setAuthor,
  setEmail,
  setTitle,
  setDescription,
  createIdea,
  createIdeaWithImage,
  resetWizard
} from "../actions";

class IdeaWizard extends React.Component {
  state = {
    author: "",
    email: "",
    title: "",
    description: "",
    file: null,
    contactMe: false,
    hasBeenSent: false
  };

  componentDidMount() {
    // https://stackoverflow.com/a/39345165/2111934
    window.scrollTo(0, 0);

    this.props.fetchCategories();
    this.props.resetWizard();
  }

  onFormSubmit = event => {
    event.preventDefault();

    this.setState({ hasBeenSent: true });

    this.props.setAuthor(this.state.author);
    this.props.setEmail(this.state.email);
    this.props.setTitle(this.state.title);
    this.props.setDescription(this.state.description);

    if (this.state.file) {
      this.props.createIdeaWithImage(this.state.file);
      return;
    }

    this.props.createIdea();
  };

  onAuthorChange = event => {
    this.setState({ author: event.target.value });
  };

  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  onTitleChange = event => {
    this.setState({ title: event.target.value });
  };

  onDescriptionChange = event => {
    this.setState({ description: event.target.value });
  };

  onFileChange = event => {
    this.setState({ file: event.target.files[0] });
  };

  // TODO: Set focus on email field when checkbox is checked.
  onContactMeChange = event => {
    const isChecked = event.target.checked;
    this.setState({ contactMe: isChecked });
    if (!isChecked) {
      this.setState({ email: "" });
    }
  };

  isSubmittingDisabled = () => {
    return !(
      this.props.wizard.categoryId &&
      this.props.wizard.subcategoryId &&
      this.props.wizard.address &&
      this.state.author &&
      this.state.title &&
      this.state.description
    );
  };

  renderCategories(categories, actionCreator, name, label) {
    if (!categories.length) {
      return null;
    }

    const labels = categories.map(category => {
      return (
        <label className="mr-radio" key={category.id}>
          <input
            onChange={() => actionCreator(category.id)}
            type="radio"
            value={category.id}
            name={name}
            className="mr-radio-input"
          />
          <span className={`mr-radio-label -${name}`}>{category.name}</span>
        </label>
      );
    });

    return (
      <div className="mr-form-group">
        <div className="row">
          <div className="col mr-form-label">
            {label}
            <span className="mr-form-label-required">*</span>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="mr-radio-group">{labels}</div>
          </div>
        </div>
      </div>
    );
  }

  renderEmailField() {
    if (!this.state.contactMe) {
      return null;
    }

    return (
      <div className="mr-form-group">
        <div className="row">
          <label htmlFor="email" className="col mr-form-label">
            Jūsu e-pasts (netiks publicēts)
          </label>
        </div>
        <div className="row">
          <div className="col-xs-12 col-sm-9 col-md-7">
            <input
              className="mr-form-control"
              id="email"
              type="text"
              value={this.state.email}
              onChange={this.onEmailChange}
            />
          </div>
        </div>
      </div>
    );
  }

  render() {
    let subcategories = [];

    if (this.props.wizard.categoryId) {
      subcategories = this.props.categories.filter(
        category => category.id === this.props.wizard.categoryId
      )[0].subcategories;
    }

    return (
      <div className="mr-page-inner">
        <header className="row mr-page-header">
          <h1 className="col mr-page-title">Iesniedz ideju</h1>
        </header>
        <form className="mr-form" onSubmit={this.onFormSubmit}>
          {this.renderCategories(
            this.props.categories,
            this.props.setCategoryId,
            "category",
            "Izvēlies grupu"
          )}

          {this.renderCategories(
            subcategories,
            this.props.setSubcategoryId,
            "subcategory",
            "Izvēlies kategoriju"
          )}

          <div className="mr-form-group">
            <div className="row">
              <label htmlFor="location" className="col mr-form-label">
                Adrese
                <span className="mr-form-label-required">*</span>
              </label>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-9 col-md-7">
                <Address nextElementIdToFocus="title" />
              </div>
            </div>
          </div>

          <div className="mr-form-group">
            <div className="row">
              <label htmlFor="title" className="col mr-form-label">
                Virsraksts
                <span className="mr-form-label-required">*</span>
              </label>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-9 col-md-7">
                <input
                  className="mr-form-control"
                  id="title"
                  type="text"
                  value={this.state.title}
                  onChange={this.onTitleChange}
                />
              </div>
            </div>
          </div>

          <div className="mr-form-group">
            <div className="row">
              <label htmlFor="description" className="col mr-form-label">
                Idejas apraksts
                <span className="mr-form-label-required">*</span>
              </label>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-9 col-md-7">
                <textarea
                  className="mr-form-control"
                  id="description"
                  value={this.state.description}
                  onChange={this.onDescriptionChange}
                />
              </div>
            </div>
          </div>

          <div className="mr-form-group">
            <div className="row">
              <label htmlFor="author" className="col mr-form-label">
                Jūsu vārds
                <span className="mr-form-label-required">*</span>
              </label>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-9 col-md-7">
                <input
                  className="mr-form-control"
                  id="author"
                  type="text"
                  value={this.state.author}
                  onChange={this.onAuthorChange}
                />
              </div>
            </div>
          </div>

          <div className="mr-form-group">
            <label className="mr-checkbox">
              <input
                type="checkbox"
                checked={this.state.contactMe}
                onChange={this.onContactMeChange}
                className="mr-checkbox-input"
              />
              <span className="mr-checkbox-label">
                Piekrītu, ka Uzlabo moderatori var ar mani sazināties, lai
                uzzinātu vairāk par šo ideju
              </span>
            </label>
          </div>

          {this.renderEmailField()}

          <div className="mr-form-group">
            <div className="row">
              <label htmlFor="image" className="col mr-form-label">
                Attēls vai idejas vizualizācija (nav obligāti)
              </label>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-9 col-md-7">
                <input
                  className="mr-form-file"
                  id="image"
                  type="file"
                  onChange={this.onFileChange}
                />
              </div>
            </div>
          </div>

          <footer className="mr-form-footer">
            <div className="row">
              <div className="col-xs-8 col-sm-5 col-md-4 col-lg-3">
                {this.state.hasBeenSent ? (
                  <div className="loader" />
                ) : (
                  <button
                    className="mr-btn -green -lg"
                    type="submit"
                    disabled={this.isSubmittingDisabled()}
                  >
                    Iesniegt
                    <span className="mr-btn-tooltip">
                      Lai iesniegtu ideju, lūdzu aizpildi veidlapu
                    </span>
                  </button>
                )}
              </div>
            </div>
          </footer>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    wizard: state.wizard,
    categories: Object.values(state.categories)
  };
};

export default connect(mapStateToProps, {
  fetchCategories,
  setCategoryId,
  setSubcategoryId,
  setAuthor,
  setEmail,
  setTitle,
  setDescription,
  createIdea,
  createIdeaWithImage,
  resetWizard
})(IdeaWizard);
