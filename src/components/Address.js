import React from "react";
import { connect } from "react-redux";
import { setAddress } from "../actions";

class Address extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: "" };
    this.addressRef = React.createRef();
  }

  onValueChange = event => {
    const address = event.target.value;
    this.setState({ value: address });
    this.props.setAddress(address);
  };

  componentDidMount() {
    const options = {
      types: ["address"],
      fields: ["name", "formatted_address"],
      componentRestrictions: {country: "lv"}
    };
    const autocomplete = new window.google.maps.places.Autocomplete(this.addressRef.current, options);

    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();
      let address = place.name;

      if (place.formatted_address) {
        address = place.formatted_address;
      }

      this.setState({ value: address });
      this.props.setAddress(address);

      const nextElementToFocus = document.getElementById(this.props.nextElementIdToFocus);
      nextElementToFocus.focus();
    });
  }

  render() {
    return (
      <div>
        <input id="location" className="mr-form-control" type="text" ref={this.addressRef} value={this.state.value} onChange={this.onValueChange} size="100" />
      </div>
    );
  }
}

export default connect(
  null,
  { setAddress }
)(Address);
