import React from "react";
import { Link } from "react-router-dom";

const Home = props => {
  return (
    <section className="mr-page-inner">
      <div className="mr-slide">
        <section className="mr-slide-inner">
          <header className="mr-slide-header">
            <h1 className="mr-slide-title">Uzlabo pilsētu!</h1>
          </header>
          <div className="mr-slide-content">
            <p>
              Vai Tev ir ideja, kā uzlabot vidi kādā no Rīgas apkaimēm?
              <br />
              Iesniedz savu priekšlikumu, savāc atbalstītājus un palīdzi to
              realizēt!
            </p>
            <p>
              Šobrīd testējam platformu VEF, Teikas, Sarkandaugavas, Imantas un
              Zolitūdes apkaimēs.
            </p>
          </div>
          <footer className="mr-slide-footer">
            <div className="mr-slide-footer-row">
              <Link to="/new" className="mr-btn -green -lg">
                Iesniedz ideju
              </Link>
            </div>
            <div className="mr-slide-footer-row">
              <Link to="/ideas" className="mr-link-accent">
                Apskatīt visas idejas
              </Link>
            </div>
          </footer>
        </section>
      </div>

      <section>
        <header className="row mr-page-header center-xs">
          <div className="col mr-page-title">Platformas darbības principi</div>
        </header>

        <div className="row">
          <div className="mr-steps col">
            <div className="row mr-steps-group">
              <div className="mr-step col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <img
                  src="/static/images/step-1.svg"
                  alt="Esi pamanījis iespēju uzlabot pilsētu?"
                  className="mr-step-img"
                />
                <div className="mr-step-description">
                  Esi pamanījis iespēju uzlabot pilsētu?
                </div>
              </div>

              <div className="mr-step col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <img
                  src="/static/images/step-2.svg"
                  alt="Iesniedz sava risinājuma ideju"
                  className="mr-step-img"
                />
                <div className="mr-step-description">
                  Iesniedz sava risinājuma ideju
                </div>
              </div>

              <div className="mr-step col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <img
                  src="/static/images/step-3.svg"
                  alt="Savāc atbalstītājus un kopīgiem spēkiem virzi ideju tuvāk realizēšanai"
                  className="mr-step-img"
                />
                <div className="mr-step-description">
                  Savāc atbalstītājus un kopīgiem spēkiem virzi ideju tuvāk
                  realizēšanai
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-12 mr-page-content">
            <p>
              Ikdienas gaitās bieži vien redzam dažādas nepilnības mūsu
              apkārtējā vidē. Platforma uzlabo.lv aicina visus Latvijas
              iedzīvotājus spert nākamo soli un iesaistīties risinājumu
              radīšanā! Ja redzi interesantu veidu kā risināt eksistējošu
              problēmu Tavā pilsētā, neturi to pie sevis, bet dalies ar pasauli.
            </p>
            <p>
              Ar uzlabo.lv palīdzību vari savākt atbalstītājus savam
              piedāvātajam risinājumam, vai arī vienkārši iesniegt savu
              priekšlikumu ideju bankā, lai to brīvi varētu izmantot ikviens
              jaunais uzņēmējs, programmētājs, pilsētas entuziasts vai
              pašvaldību darbinieks.
            </p>
            <h2>Kas mēs esam?</h2>
            <p>
              Šo portālu izveidojusi domubiedru komanda, kas iestājas par
              iedzīvotājiem draudzīgu, tīru un drošu pilsētu. Mēs esam
              brīvprātīgie programmētāji, inovāciju kustība{" "}
              <a href="https://www.vefresh.com/">VEFRESH</a>, biedrība{" "}
              <a href="https://www.pilsetacilvekiem.lv/">Pilsēta Cilvēkiem</a>{" "}
              un vairākas Rīgas apkaimju biedrības. Nākotnē kopīgiem spēkiem
              plānojam šo portālu attīstīt tālāk, un esam atvērti sadarbībai.
            </p>
          </div>
        </div>
      </section>
    </section>
  );
};

export default Home;
