import React from "react";
import { connect } from "react-redux";
import qs from "query-string";

import { fetchIdeas } from "../actions";
import IdeaStat from "./IdeaStat";
import IdeaCard from "./IdeaCard";
import Paginator from "./Paginator";

class IdeaCardList extends React.Component {
  currentPage = parseInt(qs.parse(this.props.location.search).page) || 1;

  componentDidMount() {
    // https://stackoverflow.com/a/39345165/2111934
    window.scrollTo(0, 0);

    this.props.fetchIdeas(this.currentPage);
  }

  openPage = pageNumber => {
    this.currentPage = pageNumber;
    this.props.fetchIdeas(this.currentPage);
  };

  renderCards() {
    return this.props.ideas.map(idea => {
      return (
        <IdeaCard
          key={idea.id}
          id={idea.id}
          title={idea.title}
          author={idea.author}
          address={idea.address}
          category={idea.category}
          subcategory={idea.subcategory}
          status={idea.status}
          upvotes={idea.upvotes}
          downvotes={idea.downvotes}
          created_at={idea.created_at}
        />
      );
    });
  }

  render() {
    return (
      <div className="mr-page-inner">
        <IdeaStat total={this.props.ideaCounter} />

        <div className="row">
          <div className="mr-issues col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {this.renderCards()}
          </div>
          <Paginator
            recordsPerPage={15}
            totalRecords={this.props.ideaCounter}
            currentPage={this.currentPage}
            openPage={this.openPage}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  // Calling `reverse()` is a quick and dirty solution to save the desc order of ideas.
  // Probably we should get rid of lodash and store sequences in arrays or maps
  // instead of objects because objects are not sortable.
  return {
    ideas: Object.values(state.ideas).reverse(),
    ideaCounter: state.ideaCounter
  };
};

export default connect(mapStateToProps, { fetchIdeas: fetchIdeas })(
  IdeaCardList
);
