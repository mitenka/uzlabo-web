import React from "react";
import { Link, withRouter } from "react-router-dom";
import _ from "lodash";

const Paginator = props => {
  if (!props.totalRecords) {
    return null;
  }

  const totalPages = Math.ceil(props.totalRecords / props.recordsPerPage);
  const offset = props.recordsPerPage * (props.currentPage - 1);
  const range = [
    offset + 1,
    Math.min(offset + props.recordsPerPage, props.totalRecords)
  ];

  return (
    <div className="col-xs-12">
      <div className="row">
        <div className="col-xs-12 mr-pagination">
          <div className="row middle-xs end-xs">
            <div className="col mr-pagination-description">
              {range[0]} &ndash; {range[1]} of {props.totalRecords}
            </div>
            <div className="col mr-pagination-pages">
              <Link
                to={props.location.pathname}
                onClick={() => props.openPage(1)}
                className={
                  props.currentPage === 1
                    ? "mr-pagination-page mr-pagination-arrow -disabled"
                    : "mr-pagination-page mr-pagination-arrow"
                }
              >
                <span className="mr-icon icon-arrow-first"></span>
              </Link>
              <Link
                to={
                  props.currentPage <= 2
                    ? props.location.pathname
                    : props.location.pathname + `?page=${props.currentPage - 1}`
                }
                onClick={() =>
                  props.openPage(
                    props.currentPage <= 2 ? 1 : props.currentPage - 1
                  )
                }
                className={
                  props.currentPage === 1
                    ? "mr-pagination-page mr-pagination-arrow -disabled"
                    : "mr-pagination-page mr-pagination-arrow"
                }
              >
                <span className="mr-icon icon-arrow-left"></span>
              </Link>

              {_.times(totalPages, index => {
                const pageNumber = index + 1;
                return (
                  <Link
                    to={
                      pageNumber === 1
                        ? props.location.pathname
                        : props.location.pathname + `?page=${pageNumber}`
                    }
                    onClick={() => props.openPage(pageNumber)}
                    key={pageNumber}
                    className={
                      pageNumber === props.currentPage
                        ? "mr-pagination-page -active"
                        : "mr-pagination-page"
                    }
                  >
                    {pageNumber}
                  </Link>
                );
              })}

              <Link
                to={
                  props.currentPage < totalPages
                    ? props.location.pathname + `?page=${props.currentPage + 1}`
                    : props.location.pathname + `?page=${totalPages}`
                }
                onClick={() =>
                  props.openPage(
                    props.currentPage < totalPages
                      ? props.currentPage + 1
                      : totalPages
                  )
                }
                className={
                  props.currentPage === totalPages
                    ? "mr-pagination-page mr-pagination-arrow -disabled"
                    : "mr-pagination-page mr-pagination-arrow"
                }
              >
                <span className="mr-icon icon-arrow-right"></span>
              </Link>
              <Link
                to={props.location.pathname + `?page=${totalPages}`}
                onClick={() => props.openPage(totalPages)}
                className={
                  props.currentPage === totalPages
                    ? "mr-pagination-page mr-pagination-arrow -disabled"
                    : "mr-pagination-page mr-pagination-arrow"
                }
              >
                <span className="mr-icon icon-arrow-last"></span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Paginator);
