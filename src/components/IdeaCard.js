import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import "moment/locale/lv";

moment.locale("lv");

const IdeaCard = props => {
  return (
    <Link className="mr-card row" to={`/ideas/${props.id}`}>
      <div className="mr-card-inner col">
        <header className="row mr-card-header between-xs">
          <h2 className="col mr-card-title">{props.title}</h2>
          <div className="col mr-votes">
            <div className="mr-votes-vote color-greyish">
              <span className="mr-icon icon-up-vote"></span>
              {props.upvotes}
            </div>
            <div className="mr-votes-vote color-greyish">
              <span className="mr-icon icon-down-vote"></span>
              {props.downvotes}
            </div>
          </div>
        </header>

        <div className="row">
          <div className="mr-card-content col-lg-9 col-md-9 col-sm-6 col-xs-12">
            <div className="row mr-card-address">
              <div className="col">
                <span className="mr-icon icon-Map color-greyish" />
                {props.address}
              </div>
            </div>

            <div className="row">
              <div className="col">
                <span className="mr-card-category -category">
                  {props.category.name}
                </span>
                <span className="mr-card-category -subcategory">
                  {props.subcategory.name}
                </span>
              </div>
            </div>
          </div>
        </div>

        <footer className="mr-card-footer row">
          <div className="col color-greyish">
            Pievienots: {moment(props.created_at).calendar()}
          </div>
          <div className="col color-greyish">Autors: {props.author}</div>
        </footer>
      </div>
    </Link>
  );
};

export default IdeaCard;
