import React from "react";
import { connect } from "react-redux";
import { vote } from "../actions";

class Voting extends React.Component {
  onVote = event => {
    const grade = event.currentTarget.getAttribute("data-grade");

    const votes = localStorage.getItem("votes");
    let votesObject = JSON.parse(votes);

    if (!votesObject) {
      votesObject = {};
    }

    if (this.props.idea.id in votesObject) {
      return;
    }

    votesObject[this.props.idea.id] = grade;
    localStorage.setItem("votes", JSON.stringify(votesObject));

    this.props.vote(this.props.idea, grade);
  };

  render() {
    const votes = localStorage.getItem("votes");
    let votesObject = JSON.parse(votes);
    if (!votesObject) {
      votesObject = {};
    }

    let isDisabled = false;
    let upvoteClassName = "mr-votes-vote";
    let downvoteClassName = "mr-votes-vote";
    if (this.props.idea.id in votesObject) {
      isDisabled = true;
      if (votesObject[this.props.idea.id] === "upvote") {
        upvoteClassName = "elected mr-votes-vote";
      } else if (votesObject[this.props.idea.id] === "downvote") {
        downvoteClassName = "elected mr-votes-vote";
      }
    }

    return (
      <div className="mr-votes">
        <button disabled={isDisabled} onClick={this.onVote} type="button" className={upvoteClassName} data-grade="upvote">
          <span className="mr-icon icon-up-vote" />
          {this.props.idea.upvotes}
        </button>
        <button disabled={isDisabled} onClick={this.onVote} type="button" className={downvoteClassName} data-grade="downvote">
          <span className="mr-icon icon-down-vote" />
          {this.props.idea.downvotes}
        </button>
      </div>
    );
  }
}

export default connect(null, { vote })(Voting);
