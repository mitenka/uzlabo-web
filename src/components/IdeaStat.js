import React from "react";

const IdeaStat = props => {
  if (!props.total) {
    return null;
  }

  return (
    <header className="row mr-page-header between-xs">
      <h1 className="col mr-page-title">Idejas</h1>

      <div className="col mr-page-statistic">
        <div className="mr-page-statistic-item">Kopā: {props.total}</div>
      </div>
    </header>
  );
};

export default IdeaStat;
