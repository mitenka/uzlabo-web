import React from "react";
import { Router, Route } from "react-router-dom";
import ReactGA from "react-ga";

import Header from "./Header";
import Home from "./Home";
import IdeaCardList from "./IdeaCardList";
import IdeaDetail from "./IdeaDetail";
import IdeaWizard from "./IdeaWizard";
import Footer from "./Footer";
import history from "../history";

ReactGA.initialize("UA-154453731-1");
ReactGA.pageview(window.location.pathname + window.location.search);

const App = props => {
  return (
    <div className="mr-page -bg">
      <div className="container">
        <Router history={history}>
          <React.Fragment>
            <div className="container-inner">
              <Header />
              <Route path="/" exact component={Home} />
              <Route path="/ideas" exact component={IdeaCardList} />
              <Route path="/ideas/:id" component={IdeaDetail} />
              <Route path="/new" component={IdeaWizard} />
            </div>
            <Footer />
          </React.Fragment>
        </Router>
      </div>
    </div>
  );
};

export default App;
