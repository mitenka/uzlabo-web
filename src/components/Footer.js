import React from "react";

const Footer = props => {
  return (
    <div className="mr-footer">
      <div className="row">
        <section className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mr-footer-col">
          <header className="row center-xs start-sm mr-footer-header">
            <div className="col">
              Kontakti
            </div>
          </header>
          <div className="row center-xs start-sm mr-footer-contacts mr-footer-row">
            <div className="col">
              <a href="https://fb.me/uzlabo" className="mr-footer-social-link">
                <span className="icon-Facebook" />
              </a>
            </div>
          </div>
          <div className="row center-xs start-sm mr-footer-contacts mr-footer-row">
            <div className="col">
              <a href="https://m.me/uzlabo">Sazinies ar mums</a>
            </div>
          </div>
        </section>

        <section className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mr-footer-col">
          <header className="row center-xs end-sm mr-footer-header">
            <div className="col">
              Draugi
            </div>
          </header>
          <div className="row center-xs end-sm mr-footer-row middle-xs">
            <div className="col">
              <div className="mr-footer-row">
                <a href="https://www.vefresh.com/" className="mr-footer-logo">
                  <img
                    src="/static/images/vefresh-logo.png"
                    alt="VEFRESH"
                    title="VEFRESH"
                  />
                </a>
              </div>
              <div className="mr-footer-row">
                <a href="https://www.pilsetacilvekiem.lv/" className="mr-footer-logo">
                  <img src="/static/images/pilceta-cilvekiem-logo.png" alt="Pilsēta cilvēkiem" title="Pilsēta cilvēkiem" />
                </a>
              </div>
            </div>
            <div className="col">
              <a href="http://www.sarkandaugavai.lv/" className="mr-footer-logo">
                <img src="/static/images/sarkandaugavai-logo.png" alt="Sarkandaugavas attīstības biedrība" title="Sarkandaugavas attīstības biedrība" />
              </a>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Footer;
