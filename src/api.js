import axios from "axios";

let url = "";

if (process.env.NODE_ENV === "development") {
  url = "http://127.0.0.1:8000/api";
} else if (process.env.NODE_ENV === "production") {
  url = "https://uzlabo.lv/api";
}

export default axios.create({
  baseURL: url
});
