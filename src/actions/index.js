import api from "../api";
import history from "../history";
import {
  FETCH_IDEA,
  FETCH_IDEAS,
  CREATE_IDEA,
  FETCH_CATEGORIES,
  SET_IDEA_COUNTER,
  SET_CATEGORY_ID,
  SET_SUBCATEGORY_ID,
  SET_ADDRESS,
  SET_AUTHOR,
  SET_EMAIL,
  SET_TITLE,
  SET_DESCRIPTION,
  RESET_WIZARD
} from "./types";

export const fetchIdea = id => async dispatch => {
  const response = await api.get(`/ideas/${id}/`);

  dispatch({ type: FETCH_IDEA, payload: response.data });
};

export function fetchIdeas(pageNumber = 1) {
  return async function(dispatch) {
    const response = await api.get(`/ideas/?page=${pageNumber}`);

    dispatch({ type: FETCH_IDEAS, payload: response.data.results });
    dispatch({ type: SET_IDEA_COUNTER, payload: response.data.count });
  };
}

export const fetchCategories = () => async dispatch => {
  const response = await api.get("/categories/");

  dispatch({ type: FETCH_CATEGORIES, payload: response.data });
};

export const setCategoryId = id => {
  return {
    type: SET_CATEGORY_ID,
    payload: id
  };
};

export const setSubcategoryId = id => {
  return {
    type: SET_SUBCATEGORY_ID,
    payload: id
  };
};

export const setAddress = address => {
  return {
    type: SET_ADDRESS,
    payload: address
  };
};

export const setAuthor = author => {
  return {
    type: SET_AUTHOR,
    payload: author
  };
};

export const setEmail = email => {
  return {
    type: SET_EMAIL,
    payload: email
  };
};

export const setTitle = title => {
  return {
    type: SET_TITLE,
    payload: title
  };
};

export const setDescription = description => {
  return {
    type: SET_DESCRIPTION,
    payload: description
  };
};

export const createIdeaWithImage = file => async (dispatch, getState) => {
  const formData = new FormData();
  formData.append("file", file);

  const imageResponse = await api.post("/images/", formData);

  const params = {
    images: [imageResponse.data.id],
    category: getState().wizard.categoryId,
    subcategory: getState().wizard.subcategoryId,
    address: getState().wizard.address,
    author: getState().wizard.author,
    email: getState().wizard.email,
    title: getState().wizard.title,
    description: getState().wizard.description
  };
  const response = await api.post("/ideas/", params);

  dispatch({ type: CREATE_IDEA, payload: response.data });
  dispatch({ type: RESET_WIZARD });

  history.push(`/ideas/${response.data.id}`);
};

export const createIdea = () => async (dispatch, getState) => {
  const params = {
    category: getState().wizard.categoryId,
    subcategory: getState().wizard.subcategoryId,
    address: getState().wizard.address,
    author: getState().wizard.author,
    email: getState().wizard.email,
    title: getState().wizard.title,
    description: getState().wizard.description
  };
  const response = await api.post("/ideas/", params);

  dispatch({ type: CREATE_IDEA, payload: response.data });
  dispatch({ type: RESET_WIZARD });

  history.push(`/ideas/${response.data.id}`);
};

export const resetWizard = () => {
  return {
    type: RESET_WIZARD
  };
};

export const vote = (idea, grade) => async dispatch => {
  const response = await api.post(`/${grade}/${idea.id}/`);

  dispatch({ type: FETCH_IDEA, payload: response.data });
};
