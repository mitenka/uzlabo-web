import _ from "lodash";
import { FETCH_IDEA, FETCH_IDEAS, CREATE_IDEA } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_IDEA:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_IDEAS:
      return { ..._.mapKeys(action.payload, "id") };
    case CREATE_IDEA:
      return { ...state, [action.payload.id]: action.payload };
    default:
      return state;
  }
};
