import { combineReducers } from "redux";
import ideasReducer from "./ideasReducer";
import categoriesReducer from "./categoriesReducer";
import wizardReducer from "./wizardReducer";
import ideaStatReducer from "./ideaCounterReducer";

export default combineReducers({
  ideas: ideasReducer,
  categories: categoriesReducer,
  wizard: wizardReducer,
  ideaCounter: ideaStatReducer
});
