import {
  SET_CATEGORY_ID,
  SET_SUBCATEGORY_ID,
  SET_ADDRESS,
  SET_AUTHOR,
  SET_EMAIL,
  SET_TITLE,
  SET_DESCRIPTION,
  RESET_WIZARD
} from "../actions/types";

const initialState = {
  categoryId: null,
  subcategoryId: null,
  address: "",
  author: "",
  email: "",
  title: "",
  description: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORY_ID:
      return { ...state, categoryId: action.payload, subcategoryId: null };
    case SET_SUBCATEGORY_ID:
      return { ...state, subcategoryId: action.payload };
    case SET_ADDRESS:
      return { ...state, address: action.payload };
    case SET_AUTHOR:
      return { ...state, author: action.payload };
    case SET_EMAIL:
      return { ...state, email: action.payload };
    case SET_TITLE:
      return { ...state, title: action.payload };
    case SET_DESCRIPTION:
      return { ...state, description: action.payload };
    case RESET_WIZARD:
      return initialState;
    default:
      return state;
  }
};
