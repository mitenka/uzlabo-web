import { SET_IDEA_COUNTER } from "../actions/types";

export default (state = 0, action) => {
  switch (action.type) {
    case SET_IDEA_COUNTER:
      return action.payload;
    default:
      return state;
  }
};
